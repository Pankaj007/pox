#!/usr/bin/python


class Node:
    def __init__(self):
        self.data=None
        self.left=None
        self.right=None
        self.parent=None    
    def __len__(self):
        return len(self.data)
            
        

class Btries:
           
        def __init__(self):
            print "\n creating patricia tree \n"
            self.root=Node()
            self.root.data='#'
        
      
##################################################################

        def delete(self,start,inode):
            if(start.data == "#"):
                if(inode[0]=="0"):
                    self.delete(start.left, inode)
                else:
                    self.delete(start.right, inode)
            else:
                temp=start
                l=len(temp.data)
                if(temp.data==inode[:l]):
                    if (temp.left==None and temp.right==None) :
                        prv=temp.parent
                        if(prv.data=="#"):
                            if(prv.left.data==inode[:l]):
                                prv.left=None
                            else:
                                prv.right=None
                            
                            
                        elif(prv.left.data==inode[:l]):
                            prv.left=None
                            prv.data=prv.data+prv.right.data
                            if prv.right.right!=None:
                                prv.right.left.parent=prv
                                prv.right.right.parent=prv
                                prv.left=prv.right.left
                                prv.right=prv.right.right
                                
                            else:
                                prv.right=None
                            
                        else:
                            prv.right=None
                            prv.data=prv.data+prv.left.data
                            if prv.left.left!=None:
                                prv.left.left.parent=prv
                                prv.left.right.parent=prv
                                prv.left=prv.left.left
                                prv.right=prv.left.right
                                
                            else:
                                prv.left=None
                            
                    else:
                        if(inode[l]=="0"):
                            self.delete(start.left,inode[l:])
                        else:
                            self.delete(start.right, inode[l:])
                else:
                    print "node not found "
                    
        def insert_ip(self,ip):
            print ip
            ip=ip.split(".")
            bip=""
            for x in ip:
                if int(x)>=0 and int(x)<=255:
                    p=bin(int(x))
                    p=p[2:].rjust(8,'0')
                    #print p
                    bip=bip+p
            print"inserting ip=%s"%(bip)
            self.insert(self.root,bip)            
            
        def display(self):
            level=Node()
            level.data="@@@"
            queue=[level,self.root]
            i=1
            while(len(queue)!=0):
                x=queue.pop()
                if(x.data=="@@@"):
                    print"---------------level%d"%i
                    if(len(queue)!=0):
                        i=i+1
                        queue.insert(0,level)
                else:
                    print"[{0}]".format(x.data)
                    if(x.left!=None):
                        queue.insert(0,x.left)
                    if(x.right!=None):
                        queue.insert(0,x.right)
                    
                        
                    
############################################################################################3
            
                
        def insert(self,start,inode):
            
            if(self.root.left==None and self.root.right==None):
                x=Node()
                x.data=inode
                x.parent=self.root
                if(inode[0]=="0"):
                    self.root.left=x
                else:
                    self.root.right=x
                print "root is %s\n"%self.root.data
                return
            else:
                temp=start
                i=0
                while(i<len(inode) and i<len(temp.data)):
                    if  inode[i]==temp.data[i]:
                        i=i+1
                    else:
                        break
                    #print "i=%d"%i
                #print " value of i:{0}  length of current node:{1}".format(i,len(temp.data))    
                if(i==0):
                    #print "no match "
                    x=Node()
                    x.data=inode
                    x.parent=temp
                    #print "temp {0} left{1} and right{2}".format(temp.data,temp.left,temp.right)
                    if(temp.data=='#' and temp.left!=None and temp.right==None):

                        if temp.left.data[0]==inode[0]:
                            self.insert(temp.left,inode)
                        else:
                            temp.right=x
                        return


                    if(temp.data=='#' and temp.right!=None and temp.left==None):
                        if temp.right.data[0]==inode[0]:
                            self.insert(temp.right,inode)
                        else:
                            temp.left=x
                        return 
                    if(temp.data=="#" and temp.right!=None and temp.left!=None):
                        if "1"==inode[0]:
                            self.insert(temp.right,inode)
                        else:
                            self.insert(temp.left,inode)
                        return 

                        
                elif(i<len(temp.data)):
                    label1=Node()
                    label2=Node()
                    label3=Node()
                    label1.data=temp.data[:i]
                    label2.data=temp.data[i:]
                    label3.data=inode[i:]
                    label2.parent=label3.parent=temp
                    
                    if label2.data[0]=="0":
                        label1.left=label2
                        label1.right=label3
                      
                        
                    else:
                        label1.left=label3
                        label1.right=label2

                    label2.left=temp.left
                    label2.right=temp.right
                    if(temp.left!=None):
                        temp.left.parent=label2
                    if(temp.right!=None):
                        temp.right.parent=label2
                    
                    temp.data=label1.data
                    temp.left=label1.left
                    temp.right=label1.right
                    #print "labels {0} , {1} , {2} :".format(label1.data,label2.data,label3.data)
                    #print "label1 left :{0} right:{1}  \n lable2 left:{2} right:{3}".format(label1.left,label1.right,label2.left,label2.right)
                    return
                else:
                    leftchild=temp.left
                    rightchild=temp.right
                    if i==len(temp.data):
                        if (leftchild==None and rightchild== None):
                            print "element already present "
                            return 
                        if(leftchild==None and "0"==inode[i]):
                            x=Node()
                            x.data=inode[i:]
                            x.parent=temp
                            temp.left=x
                            print "left child is null %s"%x.data
                            return
                        if(rightchild==None and "1"!=inode[i]):
                            x=Node()
                            x.data=inode[i:]
                            x.parent=temp
                            temp.right=x
                            print "right  child is null %s"%x.data
                            return
                        if(rightchild!=None and leftchild!=None):
                            if inode[i]=="0":
                                self.insert(temp.left,inode[i:])
                            else:
                                self.insert(temp.right,inode[i:])
                                

"""
t=Btries()
t.insert(t.root,"11111111")
t.insert(t.root,"11111111")
t.insert(t.root,"11111110")
t.insert(t.root,"11101111")
t.insert(t.root,"11110000")
t.insert(t.root,"00011111")
t.display()
#t.delete(t.root, "11101111") 
print "after deletion:"   
t.display()


t=Btries()
t.insert_ip("10.0.0.1")
t.insert_ip("10.0.0.3")
t.insert_ip("10.0.0.5")
t.insert_ip("10.0.0.8")
t.display() """   
  
                    
                  
                        
                        
                    
                        
                        
                      
                
                      
                

        
            
